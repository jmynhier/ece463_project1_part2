#!/bin/bash

if [[ $# -ne 1 ]]
then
    echo Usage:
    echo './run.sh <port-number>'
    exit 1
fi

executable=multi_service_server

# make sure the file is built.
make

rm -f large*.txt

# launch the server
./$executable $1 5000 &
serverid=$!

# wait for server to start.
sleep .5

# try to download the large file.
wget -qO- localhost:$1/examples/test_very_long.txt>large1.txt &

# wait just enough.
#sleep .01

# download again.
wget -qO- localhost:$1/examples/test_very_long.txt > large2.txt &

# check that both are running.
sleep .01
echo
echo "ps auxww | grep $executable"
ps auxww | grep $executable

#sleep 60
#kill -9 $serverid
