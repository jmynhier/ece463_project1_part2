CC=gcc
CFLAGS=-g -Wall -Wshadow -Werror
S_DIR=src
O_DIR=obj

### Step2
#CL=httpserver.jmynhier.o
#CL_OUT=httpserver

### Step3
#CL=httpserver_fork.jmynhier.o
#CL_OUT=httpserver_fork

### Step4
CL=multi_service_server.jmynhier.o
CL_OUT=multi_service_server
UDP=pingclient.jmynhier.o
UDP_OUT=pingclient
US=ping_server.jmynhier.o
US_OUT=ping_server

$(O_DIR)/%.o: $(S_DIR)/%.c
	$(CC) -c -o $@ $< $(CFLAGS)

server: $(O_DIR)/$(CL)
	$(CC) $^ $(CLFAGS) -o ./$(CL_OUT)

client: $(O_DIR)/$(UDP)
	$(CC) $^ $(CLFAGS) -o ./$(UDP_OUT)

ping: $(O_DIR)/$(US)
	$(CC) $^ $(CLFAGS) -o ./$(US_OUT)

.PHONY: clean
clean:
	rm -f $(CL_OUT)
	rm -f $(O_DIR)/*
