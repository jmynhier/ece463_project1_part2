/************************************************
 * Single process server file for Purdue ECE46300
 * Fall 2017. The program listens for HTTP1.0
 * get requests and sends files if available.
 * JWM
************************************************/
#include <stdio.h>
#include <stdlib.h>         // malloc/free
#include <string.h>         // strncpy/strlok
#include <netdb.h>          // hostbyname
#include <arpa/inet.h>      // htons/l
#include <netinet/in.h>     // sockaddr_in
#include <unistd.h>         // read/write/close
#include <sys/types.h>
#include <sys/socket.h>

#define BUFF_SIZE 512

#define HTTP200 "HTTP/1.0 200 OK\r\n\r\n"
#define HTTP404 "HTTP/1.0 404 Not Found\r\n\r\n"
#define HTTP403 "HTTP/1.0 403 Forbidden\r\n\r\n"

/* Binds the socket to the specified port. 
 * Assumes server_id is valid. On success returns
 * zero.                                     */
int sock_connect(int server_id, int serverport) {
    // Set up the socket struct.
    struct sockaddr_in sock_info = (const struct sockaddr_in){ 0 };
    sock_info.sin_family = AF_INET;
    sock_info.sin_addr.s_addr = INADDR_ANY;
    sock_info.sin_port = htons(serverport);
     
    // Bind the port.
    if (bind(server_id, 
                (struct sockaddr *) &sock_info, 
                sizeof(sock_info)) < 0) {
        // FAIL 4: No connection.
        return 4;
    }

    // Listen to the port.
    if (listen(server_id, 10) != 0) {
        // FAIL 5: Can't listen.
        return 5;
    }

    printf("Connected to %x:%i\n", sock_info.sin_addr.s_addr, serverport);

    return 0;
}

// Send HTTP responses. Can it respond meaningfully to
// a failed write?
void respond(int client_id, char *buff, int code) {
    int msg_len = 0;
    if (code == 200) {
        msg_len = sprintf(buff, HTTP200);
    } else if (code == 404) {
        msg_len = sprintf(buff, HTTP404);
    } else { // Assume 403
        msg_len = sprintf(buff, HTTP403);
    }

    write(client_id, buff, msg_len);
}

// Respond to a client's read request.
void client_request(int client_id) {
    char *buff = malloc(BUFF_SIZE);
    if (buff == NULL) {
        return;
    }

    // Read the message
    if (read(client_id, buff, BUFF_SIZE) == 0) {
        free(buff);
        return;
    }

    // Split along spaces. check each element,
    // get the filepath.
    char *slice = strtok(buff, " \r\n");
    if (slice == NULL || ((strcmp(slice, "GET") != 0) && \
            (strcmp(slice, "get") != 0))) {

        free(buff);
        return;
    }

    if ((slice = strtok(NULL, " \r\n")) == NULL) {
        free(buff);
        return;
    }

    // Store filepath
    char *file_path = malloc(strlen(slice) + 1);
    if (file_path == NULL) {
        free(buff);
        return;
    }
    file_path[0] = '.';
    strcpy((char *)&(file_path[1]), slice);
    
    // Verify valid HTTP request.
    slice = strtok(NULL, " \r\n");
    if (slice == NULL || \
            ((strcmp("HTTP/1.0", slice) != 0) && \
            (strcmp("http/1.0", slice) != 0) && \
            (strcmp("HTTP/1.1", slice) != 0) && \
            (strcmp("http/1.1", slice) != 0))) {
        free(file_path);
        free(buff);
        return;
    }

    // Check that the file exits.
    int file_exists = access(file_path, F_OK) != -1;
    
    // Try to open the file.
    FILE *fin = fopen(file_path, "r");
    free(file_path);
    if (fin == NULL)  {
        if (file_exists) {
            // Not readable.
            respond(client_id, buff, 403);
        } else { 
            // Doesn't exist (or other fopen error...)
            respond(client_id, buff, 404);
        }
        free(buff);
        return;
    }

    // Confirm request.
    respond(client_id, buff, 200);

    printf("Sending file.\n");

    // Read into buffer, then write to client
    int write_size = 0;
    while((write_size = fread(buff, sizeof(char), BUFF_SIZE, fin)) != 0) {
        write(client_id, buff, write_size);
    }
    
    // End connection.
    free(buff);
}

int main(int argc, char *argv[]) {
    // Check inputs.
    if (argc != 2) {
        // FAIL 1: Wrong number of args.
        return 1;
    }
    int sport;
    if (sscanf(argv[1], " %i", &sport) != 1) {
        //FAIL 2: Invalid port number.
        return 2;
    }

    // Create the socket.
    int server_id;
    if ((server_id = socket(AF_INET, 
                         SOCK_STREAM, 
                         0)) < 0) {
        // FAIL 3: Socket creation.
        return 3;
    }

    // Bind the socket to the port and listen to it.
    int valid_sock = sock_connect(server_id, sport);
    if (valid_sock != 0) {
        close(server_id);
        return valid_sock;
    }

    // Keep accepting connections.
    struct sockaddr_in *client_sock = NULL;
    int sock_length = sizeof(struct sockaddr_in);
    while (1) {
        printf("Waiting for client\n");
        int client_id = accept(server_id, 
                    (struct sockaddr *)client_sock,
                    (socklen_t *)&sock_length);

        printf("Connected to a client\n");

        client_request(client_id);

        printf("Ended connection.\n");
        close(client_id);
    }
    close(server_id);
    return 0;
}
