/************************************************
 * UDP simple test server.
 * JWM
************************************************/
#include <stdio.h>
#include <stdlib.h>         // malloc/free
#include <string.h>         // strncpy/strlok
#include <netdb.h>          // hostbyname
#include <arpa/inet.h>      // htons/l
#include <netinet/in.h>     // sockaddr_in
#include <unistd.h>         // read/write/close
#include <sys/types.h>
#include <sys/socket.h>

#define PING_BUFF_SIZE 68

int main(int argc, char **argv) {
    if (argc != 2) {
        printf("Invalid arguments!\n");
        return 1;
    }

    int port;
    if (sscanf(argv[1], " %i", &port) < 1) {
        printf("Not a valid port\n");
        return 2;
    }

    int server_id = socket(AF_INET, SOCK_DGRAM, 0);
    if (server_id < 0) {
        printf("Failed to make socket!");
        return 3;
    }

    struct sockaddr_in server_addr = (const struct sockaddr_in){ 0 };
    server_addr.sin_family = AF_INET;
    server_addr.sin_addr.s_addr = htonl(INADDR_ANY);
    server_addr.sin_port = htons((unsigned short) port);

    if (bind(server_id, (struct sockaddr *) &server_addr, sizeof(server_addr)) != 0) {
        printf("Failed to bind socket!");
    close(server_id);
        return 4;
    }

    struct sockaddr_in client_addr;
    int client_size = sizeof(client_addr);

    printf("Start waiting for messages");
    fflush(NULL);
    char buff[PING_BUFF_SIZE];
    recvfrom(server_id, (char *)buff, PING_BUFF_SIZE, 0,
            (struct sockaddr *) &client_addr, 
            (socklen_t *) &client_size);

    printf("Message recieved: %s", buff);

    close(server_id);
    return 0;
}
