/************************************************
 * Single process server file for Purdue ECE46300
 * Fall 2017. The program listens for HTTP1.0
 * get requests and sends files if available.
 * It spawns processes to handle multple requests.
 * JWM
************************************************/
#include <stdio.h>
#include <stdlib.h>         // malloc/free
#include <string.h>         // strncpy/strlok
#include <netdb.h>          // hostbyname
#include <arpa/inet.h>      // htons/l
#include <netinet/in.h>     // sockaddr_in
#include <unistd.h>         // read/write/close
#include <sys/types.h>
#include <sys/socket.h>

#define BUFF_SIZE 512

#define PING_BUFF_SIZE 68

#define HTTP200 "HTTP/1.0 200 OK\r\n\r\n"
#define HTTP404 "HTTP/1.0 404 Not Found\r\n\r\n"
#define HTTP403 "HTTP/1.0 403 Forbidden\r\n\r\n"

/* Binds the socket to the specified port. 
 * Assumes server_id is valid. On success returns
 * zero.                                     */
int sock_connect(int server_id, int serverport) {
    // Set up the socket struct.
    struct sockaddr_in sock_info = (const struct sockaddr_in){ 0 };
    sock_info.sin_family = AF_INET;
    sock_info.sin_addr.s_addr = INADDR_ANY;
    sock_info.sin_port = htons(serverport);
     
    // Bind the port.
    if (bind(server_id, 
                (struct sockaddr *) &sock_info, 
                sizeof(sock_info)) < 0) {
        // FAIL 6: No connection.
        return 6;
    }

    printf("Connected to %x:%i\n", sock_info.sin_addr.s_addr, serverport);

    return 0;
}

// Send HTTP responses. Can it respond meaningfully to
// a failed write?
void respond(int client_id, char *buff, int code) {
    int msg_len = 0;
    if (code == 200) {
        msg_len = sprintf(buff, HTTP200);
    } else if (code == 404) {
        msg_len = sprintf(buff, HTTP404);
    } else { // Assume 403
        msg_len = sprintf(buff, HTTP403);
    }

    write(client_id, buff, msg_len);
}

// Respond to a client's read request.
void client_request(int client_id) {
    char *buff = malloc(BUFF_SIZE);
    if (buff == NULL) {
        return;
    }

    // Read the message
    if (read(client_id, buff, BUFF_SIZE) == 0) {
        free(buff);
        return;
    }

    // Split along spaces. check each element,
    // get the filepath.
    char *slice = strtok(buff, " \r\n");
    if (slice == NULL || ((strcmp(slice, "GET") != 0) && \
            (strcmp(slice, "get") != 0))) {

        free(buff);
        return;
    }

    if ((slice = strtok(NULL, " \r\n")) == NULL) {
        free(buff);
        return;
    }

    // Store filepath
    char *file_path = malloc(strlen(slice) + 1);
    if (file_path == NULL) {
        free(buff);
        return;
    }
    file_path[0] = '.';
    strcpy((char *)&(file_path[1]), slice);
    
    // Verify valid HTTP request.
    slice = strtok(NULL, " \r\n");
    if (slice == NULL || \
            ((strcmp("HTTP/1.0", slice) != 0) && \
            (strcmp("http/1.0", slice) != 0) && \
            (strcmp("HTTP/1.1", slice) != 0) && \
            (strcmp("http/1.1", slice) != 0))) {
        free(file_path);
        free(buff);
        return;
    }

    // Check that the file exits.
    int file_exists = access(file_path, F_OK) != -1;
    
    // Try to open the file.
    FILE *fin = fopen(file_path, "r");
    free(file_path);
    if (fin == NULL)  {
        if (file_exists) {
            // Not readable.
            respond(client_id, buff, 403);
        } else { 
            // Doesn't exist (or other fopen error...)
            respond(client_id, buff, 404);
        }
        free(buff);
        return;
    }

    // Confirm request.
    respond(client_id, buff, 200);

    printf("Sending file.\n");

    // Read into buffer, then write to client
    int write_size = 0;
    while((write_size = fread(buff, sizeof(char), BUFF_SIZE, fin)) != 0) {
        write(client_id, buff, write_size);
    }

    // End connection.
    free(buff);
}

// Check for a valid ping message and respond.
void ping_request(int ping_id) {
    char buff[PING_BUFF_SIZE];
    struct sockaddr_in client_sock;
    int sock_len = sizeof(client_sock);

    // Read the message
    int bytes_read = recvfrom(ping_id, (char *)buff, PING_BUFF_SIZE,
                              0, (struct sockaddr *) &client_sock,
                              (socklen_t *)&sock_len); 
    if (bytes_read < 4) {
        return;
    }

    // Verify the message
    int i, code = 0;
    for (i=0; i<4; i++) {
        code |= buff[i] << 8*i;
    }
    code = ntohl(code);
    code++;
    code = htonl(code);

    // Encode the ack.
    for (i=3; i>-1; i--) {
        buff[i] = code;
        code <<= 8;
    }

    // Send the ack.
    sendto(ping_id, (char *)buff, bytes_read, 0, 
           (struct sockaddr *) &client_sock, sock_len);
}

// Create, bind, listen to a socket.
// type should be SOCK_DGRAM or SOCK_STREAM.
// Returns the socket id if it succeeds.
// returns number < 1 if failed.
int setup_socket(int port, int type) {
    int sock_id;
    if ((sock_id = socket(AF_INET, 
                         type, 
                         0)) < 0) {
        return -1;
    }

    // Bind the socket to the port.
    int valid_sock = sock_connect(sock_id, port);
    if (valid_sock != 0) {
        close(sock_id);
        return -1;
    }

    // Listen to the port.
    if (type != SOCK_DGRAM) {
        if (listen(sock_id, 10) != 0) {
            // FAIL 7: Can't listen.
            return 7;
        }
    }

    return sock_id;
}

int main(int argc, char *argv[]) {
    // Check inputs.
    if (argc != 3) {
        // FAIL 1: Wrong number of args.
        return 1;
    }
    int http_port;
    if (sscanf(argv[1], " %i", &http_port) != 1) {
        //FAIL 2: Invalid port number.
        return 2;
    }
    int ping_port;
    if (sscanf(argv[2], " %i", &ping_port) != 1) {
        //FAIL 3: Invalid port number.
        return 3;
    }

    // Create the http socket.
    int http_id = setup_socket(http_port, SOCK_STREAM);
    if (http_id < 0) {
        //FAIL 4: Invalid port number.
        return 4;
    }

    // Create the ping socket.
    int ping_id = setup_socket(ping_port, SOCK_DGRAM);
    if (ping_id < 0) {
        //FAIL 5: Invalid port number.
        close(http_id);
        return 5;
    }

    // Set up the client sock.
    struct sockaddr_in *client_sock = NULL;
    int sock_length = sizeof(struct sockaddr_in);

    // Set up the read only and mutable socket sets
    // for switching.
    fd_set read_only_set, mutable_set;
    FD_ZERO(&read_only_set);
    FD_SET(http_id, &read_only_set);
    FD_SET(ping_id, &read_only_set);
    int set_max = (http_id > ping_id ? http_id : ping_id) + 1;

    // Wait for read availability and service clients.
    while (1) {
        mutable_set = read_only_set;
        if (select(set_max, &mutable_set, NULL, NULL, NULL) < 0) {
            //FAIL 8: no selection.
            close(http_id);
            close(ping_id);
            return 8;
        }

        if (FD_ISSET(ping_id, &mutable_set)) {
            // Ping serice.
            
            //DEBUG
            printf("Ping recieved\n");

            ping_request(ping_id);
        } else if (FD_ISSET(http_id, &mutable_set)) {
            // HTTP service.
            
            //DEBUG
            printf("http request recieved\n");

            int client_id = accept(http_id, 
                        (struct sockaddr *)client_sock,
                        (socklen_t *)&sock_length);

            if (fork() == 0) {
                client_request(client_id);

                close(client_id);
                exit(0);
            }
            close(client_id);
        }
    }
    close(http_id);
    close(ping_id);
    return 0;
}
