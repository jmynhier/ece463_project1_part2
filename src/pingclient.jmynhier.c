/************************************************
 * Test UDP ping client.
 * JWM
************************************************/
#include <stdio.h>
#include <stdlib.h>         // malloc/free
#include <string.h>         // strncpy
#include <netdb.h>          // hostbyname
#include <arpa/inet.h>      // htons/l
#include <netinet/in.h>     // sockaddr_in
#include <unistd.h>         // read/write/close
#include <sys/types.h>
#include <sys/socket.h>

#define BUFF_SIZE 512

/* Connects the socket to the specified host. 
 * Assumes sockid is valid. On success returns
 * zero.                                     */
/* Executes a buffered read of the HTTP response
 * and prints it to stdout. If successful it
 * returns 0.                                   */
int ping_server(int sockid, char *hostname, int hostport, char *message) {
    // Set up the socket struct.
    struct sockaddr_in sock_info = (const struct sockaddr_in){ 0 };
    sock_info.sin_family = AF_INET;
    struct hostent *host; 
    if ((host = gethostbyname(hostname)) == NULL) {
        // FAIL 4: Host IP not found.
        return 4;
    }

    strncpy((char *)&sock_info.sin_addr.s_addr,
            (char *)host->h_addr, 
            host->h_length);
    sock_info.sin_port = htons(hostport);
     
    char *buff = malloc(BUFF_SIZE);
    if (buff == NULL) {
        // FAIL 5: Bad alloc
        return 5;
    }

    // Write 1 to buffer.
    int i;
    for (i=0; i<4; i++) {
        buff[i] = ' ';
    }

    // write message to buffer.
    sprintf(&(buff[4]), "%s", message);

    // Send the ping.
    unsigned int sock_size = sizeof(sock_info);
    printf("Sending message\n");
    printf("Response recieved: ");
    for (i=0; i<4; i++) {
        printf("%0i:", buff[i]);
    }
    printf(" %s\n", &(buff[4]));
    
    sendto(sockid, buff, strlen(buff), 0,
            (struct sockaddr *) &sock_info, sock_size);

    // Read the response
    recvfrom(sockid, buff, BUFF_SIZE, 0,
            (struct sockaddr *) &sock_info, &sock_size);
    printf("Response recieved: ");
    for (i=0; i<4; i++) {
        printf("%0i:", buff[i]);
    }
    printf(" %s\n", &(buff[4]));
    

    free(buff);    
    return 0;
}

int main(int argc, char *argv[]) {
    // Check inputs.
    if (argc != 4) {
        // FAIL 1: Wrong number of args.
        return 1;
    }
    int hport;
    if (sscanf(argv[2], " %i", &hport) != 1) {
        //FAIL 2: Invalid port number.
        return 2;
    }

    // Create the socket.
    int sockid;
    if ((sockid = socket(AF_INET, 
                         SOCK_DGRAM, 
                         0)) < 0) {
        // FAIL 3: Socket creation.
        return 3;
    }

    // Connect and ping.
    int return_value = ping_server(sockid, argv[1], hport, argv[3]);
    close(sockid);

    return return_value;
}
